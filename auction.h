#ifndef _AUCTION_H_
#define _AUCTION_H_
#include <string>
#include <iostream>

class Auction{
 protected:
  Player *plist;
  int mlist[8];
  Player *curPlayer;
  int bidlist[8];
  int maxbid;
  int curWinner;
  int propPos;
  std::string building;
 public:
  Auction(Player *curPlayer, std::string building, int propPos);
  ~Auction();
  void joinAuction(int ind);
  void leaveAuction(int ind);
  void startAuction();
  int isWon();
};

#endif
